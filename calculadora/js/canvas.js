var Canvas = document.getElementById('miCanvas');

var ancho = Canvas.width ;
var alto = Canvas.height ;

function dibujar() {

    if (Canvas.getContext) {

        Ctx = Canvas.getContext('2d');
        Ctx.clearRect(0,0,ancho,alto) ;

        ejes()
        mostrarFuncion(preparar()) ;

    }
}

function preparar() {
    var funcion = document.getElementById('entradaTexto').value;

    funcion = funcion.replaceAll('log(', 'Math.log10(')
    funcion = funcion.replaceAll('ln(', 'Math.log(')
    funcion = funcion.replaceAll('sin(', 'Math.sin(')
    funcion = funcion.replaceAll('cos(', 'Math.cos(')
    funcion = funcion.replaceAll('tan(', 'Math.tan(')
    funcion = funcion.replaceAll('√(', 'Math.sqrt(')

    for (var i=0; i<=funcion.length; i++){
        if (funcion.charAt(i)=='x'){
            if (funcion.indexOf('x') !=0){
                if (funcion.charAt(i-1) !='+'&& funcion.charAt(i-1) !='(' &&
                    funcion.charAt(i-1) !='-' && funcion.charAt(i-1) !='/' &&
                    funcion.charAt(i-1) !='*' && funcion.charAt(i-1) !='^'){

                    var funcionParte2 = funcion.substring(i)
                    var funcionParte1 = funcion.slice(0,i)
                    funcionParte2 = funcionParte2.replace('x','*x')
                    funcion = funcionParte1+funcionParte2;
                }
            }
        }
    }

    while (funcion.indexOf('^') != -1){

        var signo = funcion.indexOf('^')
        var operador1 =-1
        var operador2 =-1
        var n1,n2
        var par1 = false
        var par2 = false
        var par11, par21

        for (var i=signo-1; i>=0; i--){
            if (funcion.charAt(i) == "+" || funcion.charAt(i) == "-" || funcion.charAt(i) == '*' || funcion.charAt(i) == '/' ||
                funcion.charAt(i) == ')'  || funcion.charAt(i) == '(' || funcion.charAt(i) == '^'){
                operador1 = i;
                if (funcion.charAt(i)==')'){
                    par1 = true;
                }
                i=-1;
            }
        }

        for (var i=signo+1; i<funcion.length; i++){
            if (funcion.charAt(i) == '+' || funcion.charAt(i) == '-' || funcion.charAt(i) == '*' || funcion.charAt(i) == '/' ||
                funcion.charAt(i) == ')'  || funcion.charAt(i) == '(' || funcion.charAt(i) == '^'){
                operador2 = i;
                if (funcion.charAt(i)=='('){
                    par2 = true;
                }
                i=funcion.length+1;
            }
        }

        if (par1 == true){
            for (var i=operador1; i>=0; i--){
                if (funcion.charAt(i) == '('){
                    par11 = i
                    i = -1
                }
            }
            n1 = funcion.substring(par11+1,operador1)
        }else {
            if(operador1 == -1){
                n1 = funcion.substring(0,signo)
            }else {
                n1 = funcion.substring(operador1+1, signo);
            }
        }

        if (par2 == true){
            for (var i=operador2; i<funcion.length; i++){
                if (funcion.charAt(i) == ')'){
                    par21 = i
                    i = funcion.length+1
                }
            }
            n2 = funcion.substring(operador2+1, par21)
        }else {
            if (operador2 == -1) {
                n2 = funcion.substring(signo + 1)
            } else {
                n2 = funcion.substring(signo + 1, operador2);
            }
        }

        var cadena

        var solucion = 'Math.pow(('+n1+'),('+n2+'))';

        if (par1 == true && par2 == true){
            cadena = '('+n1.toString()+')^('+n2.toString()+')';
        }else if (par1 == true && par2 == false){
            cadena = '('+n1.toString()+')^'+n2.toString();
        }else if (par1 == false && par2 == true){
            cadena = n1.toString()+'^('+n2.toString()+')';
        } else {
            cadena = n1.toString()+'^'+n2.toString();
        }
        funcion = funcion.replace(cadena, solucion.toString());
    }

    return funcion;
}

function ejes() {

    var espacioBarras = ancho/20;
    Ctx.save();
    Ctx.lineancho = 2;

    Ctx.beginPath() ;
    Ctx.moveTo(ancho/2,0) ;
    Ctx.lineTo(ancho/2, alto);
    Ctx.strokeStyle = 'white';
    Ctx.stroke() ;

    Ctx.beginPath() ;
    Ctx.moveTo(0,alto/2) ;
    Ctx.lineTo(ancho,alto/2) ;
    Ctx.stroke() ;

    // Pintar rallas
    for (var i = 1; i <=20  ; ++i) {
        Ctx.beginPath() ;
        Ctx.moveTo(espacioBarras*i, (alto/2)-5) ;
        Ctx.lineTo(espacioBarras*i,(alto/2)+5) ;
        Ctx.strokeStyle = 'white';
        Ctx.stroke() ;

        Ctx.beginPath() ;
        Ctx.moveTo((ancho/2)-5,espacioBarras*i) ;
        Ctx.lineTo((ancho/2)+5, espacioBarras*i) ;
        Ctx.strokeStyle = 'white';
        Ctx.stroke() ;
    }
    Ctx.restore() ;
}

function mostrarFuncion(f) {

    var longitudSistemaReferencia = 20;
    var maximoX = 10
    var maximoY = 10
    var minimoX = -10
    var minimoY = -10

    function XaPixeles(x) {
        XaPixel = (x - minimoX) /longitudSistemaReferencia * ancho ;
        return XaPixel;
    }
    function YaPixeles(y) {
        YaPixel = alto - (y - minimoY) / (maximoX - minimoY) * alto ;
        return YaPixel;
    }

    var intervaloPuntos = 0.01;
    //Ctx.beginPath() ;
    //var primero = true;

    for (var x = minimoX; x <= maximoX; x += intervaloPuntos) {

        var y = eval(f.replace('x',x.toString())) ;

        // Metodo de puntos. Mas feo. Comentar dos lineas de abajo para usar el otro metodo
        Ctx.fillStyle = "white";
        Ctx.fillRect(XaPixeles(x),YaPixeles(y),1,1)
        
        //Metodo de lineas. Mas bonito pero en funciones con tangentes crea lineas verticales indeseadas.
        // Descomentar lo siguiente y las dos lineas de arriba
        /*Ctx.strokeStyle = "white";
        if (primero){
            Ctx.moveTo(XaPixeles(x),YaPixeles(y)) ;
        }else {
            Ctx.lineTo(XaPixeles(x),YaPixeles(y)) ;
        }
        primero = false*/
    }
    Ctx.stroke() ;
}