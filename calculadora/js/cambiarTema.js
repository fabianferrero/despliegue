function cambiarTema(tema){
    var estilos = document.getElementById('tema');

    if (tema == 'oscuro'){
        estilos.href = 'css/temas/oscuro.css';
    }else if (tema == 'azulito'){
        estilos.href = 'css/temas/azulito.css';
    }else if (tema == 'morado'){
        estilos.href = 'css/temas/morado.css';
    }
}