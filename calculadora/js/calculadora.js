function teclas(id){
    var resultado

    resultado = document.getElementById('pantalla')

    var contenido = resultado.value

    if (contenido.length <= 15){
        switch (id) {

            case 'uno':
                resultado.value = contenido + "1"
                break;
            case 'dos':
                resultado.value = contenido + "2"
                break;
            case 'tres':
                resultado.value = contenido + "3"
                break;
            case 'cuatro':
                resultado.value = contenido + "4"
                break;
            case 'cinco':
                resultado.value = contenido + "5"
                break;
            case 'seis':
                resultado.value = contenido + "6"
                break;
            case 'siete':
                resultado.value = contenido + "7"
                break;
            case 'ocho':
                resultado.value = contenido + "8"
                break;
            case 'nueve':
                resultado.value = contenido + "9"
                break;
            case 'cero':
                resultado.value = contenido + "0"
                break;
            case 'limpiar':
                resultado.value = ""
                break;
            case 'del':
                if (resultado.value.length != 1){
                    resultado.value = contenido.slice(0, -1)
                }else {
                    resultado.value = ''
                }
                break;
            case 'suma':
                if (resultado.value.length != 0){
                    if (resultado.value.charAt(resultado.value.length-1) != '+' && resultado.value.charAt(resultado.value.length-1) != 'x' &&
                        resultado.value.charAt(resultado.value.length-1) != '^' && resultado.value.charAt(resultado.value.length-1) != '-' &&
                        resultado.value.charAt(resultado.value.length-1) != "/"){
                        resultado.value = contenido + "+"
                    }
                }
                break;
            case 'resta':
                if (resultado.value.length==1 && resultado.value.charAt(0) == '-'){

                }else {
                    if (resultado.value.substring(resultado.value.length-2) != '--' && resultado.value.substring(resultado.value.length-2) != '+-' &&
                        resultado.value.substring(resultado.value.length-2) != 'x-' && resultado.value.substring(resultado.value.length-2) != '/-' &&
                        resultado.value.substring(resultado.value.length-2) != '^-'){
                        resultado.value = resultado.value+'-'
                    }
                }
                break;
            case 'multiplicacion':
                if (resultado.value.length != 0){
                    if (resultado.value.charAt(resultado.value.length-1) != '+' && resultado.value.charAt(resultado.value.length-1) != 'x' &&
                        resultado.value.charAt(resultado.value.length-1) != '^' && resultado.value.charAt(resultado.value.length-1) != '-' &&
                        resultado.value.charAt(resultado.value.length-1) != "/"){
                        resultado.value = contenido + "x"
                    }
                }
                break;
            case 'division':
                if (resultado.value.length != 0){
                    if (resultado.value.charAt(resultado.value.length-1) != '+' && resultado.value.charAt(resultado.value.length-1) != 'x' &&
                        resultado.value.charAt(resultado.value.length-1) != '^' && resultado.value.charAt(resultado.value.length-1) != '-' &&
                        resultado.value.charAt(resultado.value.length-1) != "/"){
                        resultado.value = contenido + "/"
                    }
                }
                break;
            case 'coma':
                if (resultado.value.substring(resultado.value.length-1) != ',') {
                    var signo = 0
                    var coma = 0
                    for (var i = resultado.value.length; i >= 0; i--) {
                        if (resultado.value.charAt(i) == '+' || resultado.value.charAt(i) == '-' ||
                            resultado.value.charAt(i) == 'x' || resultado.value.charAt(i) == '/' ||
                            resultado.value.charAt(i) == '(' || resultado.value.charAt(i) == ')' ||
                            resultado.value.charAt(i) == '^') {
                            signo = i
                            i = -1
                        }
                    }
                    for (var i = resultado.value.length; i >= 0; i--) {
                        if (resultado.value.charAt(i) == ',') {
                            coma = i
                            i = -1
                        }
                    }
                    if (signo >= coma) {
                        resultado.value = contenido + ","
                    }
                }
                break;
            case 'igual':
                var cuenta
                cuenta = contenido;

                var resultadoFinal = operaciones(cuenta).toString();
                resultado.value = resultadoFinal.replace('.',',');
                resultado.value = resultadoFinal.replace('.',',');
                if (cuenta == "0" || cuenta == ""){
                    resultado.value = "0";
                }
                break;
            case 'sin':
                resultado.value = contenido + "sin("
                break;
            case 'cos':
                resultado.value = contenido + "cos("
                break;
            case 'tan':
                resultado.value = contenido + "tan("
                break;
            case 'par1':
                resultado.value = contenido + "("
                break;
            case 'par2':
                resultado.value = contenido + ")"
                break;
            case 'pi':
                resultado.value = contenido + "π"
                break;
            case 'pi':
                resultado.value = contenido + "π"
                break;
            case 'e':
                resultado.value = contenido + "E"
                break;
            case 'pow':
                resultado.value = contenido + "^"
                break;
            case 'abs':
                resultado.value = contenido + "abs("
                break;
            case 'log10':
                resultado.value = contenido + "log("
                break;
            case 'log2':
                resultado.value = contenido + "log2("
                break;
            case 'ln':
                resultado.value = contenido + "ln("
                break;
            case 'raiz2':
                resultado.value = contenido + "√("
                break;
            case 'nraiz':
                resultado.value = contenido + "√("
                break;
            case 'signo':
                resultado.value = cambiarSigno(resultado.value)
                break;
        }
    }else {
        switch (id) {

            case 'limpiar':
                resultado.value = ""
                break;
            case 'del':
                resultado.value = contenido.slice(0, -1)
                break;
            case 'igual':
                var cuenta
                cuenta = contenido;
                resultado.value = operaciones(cuenta)
                break;
        }
    }
}
function operaciones(cuenta) {
    var resultado = cuenta

    resultado = resultado.replaceAll('x', '*')
    resultado = resultado.replaceAll('--', '+')
    resultado = resultado.replaceAll('log(', 'Math.log10(')
    resultado = resultado.replaceAll('log2(', 'Math.log2(')
    resultado = resultado.replaceAll('ln(', 'Math.log(')
    resultado = resultado.replaceAll('sin(', 'Math.sin(')
    resultado = resultado.replaceAll('cos(', 'Math.cos(')
    resultado = resultado.replaceAll('tan(', 'Math.tan(')
    resultado = resultado.replaceAll('π', 'Math.PI')
    resultado = resultado.replaceAll('E', 'Math.E')
    resultado = resultado.replaceAll('abs(', 'Math.abs(')
    resultado = resultado.replaceAll(',', '.')

    while (resultado.indexOf('^') != -1){

        var signo = resultado.indexOf('^')
        var operador1 =-1
        var operador2 =-1
        var n1,n2
        var par1 = false
        var par2 = false
        var par11, par21

        for (var i=signo-1; i>=0; i--){
            if (resultado.charAt(i) == "+" || resultado.charAt(i) == "-" || resultado.charAt(i) == 'x' || resultado.charAt(i) == '/' ||
                resultado.charAt(i) == ')'  || resultado.charAt(i) == '(' || resultado.charAt(i) == '^'){
                operador1 = i;
                if (resultado.charAt(i)==')'){
                    par1 = true;
                }
                i=-1;
            }
        }

        for (var i=signo+1; i<resultado.length; i++){
            if (resultado.charAt(i) == '+' || (resultado.charAt(i) == '-' && i!=signo+1)|| resultado.charAt(i) == 'x' || resultado.charAt(i) == '/' ||
                resultado.charAt(i) == ')'  || resultado.charAt(i) == '(' || resultado.charAt(i) == '^'){
                operador2 = i;
                if (resultado.charAt(i)=='('){
                    par2 = true;
                }
                i=resultado.length+1;
            }
        }

        if (par1 == true){
            for (var i=operador1; i>=0; i--){
                if (resultado.charAt(i) == '('){
                    par11 = i
                    i = -1
                }
            }
            n1 = resultado.substring(par11+1,operador1)
        }else {
            if(operador1 == -1){
                n1 = resultado.substring(0,signo)
            }else {
                n1 = resultado.substring(operador1+1, signo);
            }
        }

        if (par2 == true){
            for (var i=operador2; i<resultado.length; i++){
                if (resultado.charAt(i) == ')'){
                    par21 = i
                    i = resultado.length+1
                }
            }
            n2 = resultado.substring(operador2+1, par21)
        }else {
            if (operador2 == -1) {
                n2 = resultado.substring(signo + 1)
            } else {
                n2 = resultado.substring(signo + 1, operador2);
            }
        }
        var cadena

        var solucion = eval('Math.pow(('+n1+'),('+n2+'))');

        if (par1 === true && par2 === true){
            cadena = '('+n1.toString()+')^('+n2.toString()+')';
        }else if (par1 == true && par2 == false){
            cadena = '('+n1.toString()+')^'+n2.toString();
        }else if (par1 == false && par2 == true){
            cadena = n1.toString()+'^('+n2.toString()+')';
        } else {
            cadena = n1.toString()+'^'+n2.toString();
        }
        resultado = resultado.replace(cadena, solucion.toString());
    }
    while (resultado.indexOf('√') != -1) {

        var signo = resultado.indexOf('√');
        var operador = -1;
        var exponente=-1;

        for (var i = signo - 1; i >= 0; i--) {
            if (resultado.charAt(i) == "+" || resultado.charAt(i) == "-" || resultado.charAt(i) == 'x' || resultado.charAt(i) == '/' ||
                resultado.charAt(i) == '^') {
                operador = i;
                i = -1;
            }
        }

        for (var i = signo + 1; i < resultado.length; i++) {
            if (resultado.charAt(i) == ')') {
                parentesisCerrado = i;
                i = resultado.length + 1;
            }
        }


        if(operador == signo-1){
            resultado=resultado.replace("√(","Math.sqrt(");
        }else if(operador ==-1){
            exponente=resultado.substring(0,signo);
            var interior = resultado.substring(signo+2,parentesisCerrado);
            resultado=resultado.replace(exponente+"√("+interior,"Math.pow("+interior+",1/"+exponente.toString());
        }else {
            exponente=resultado.substring(operador+1,signo);
            var interior = resultado.substring(signo+2,parentesisCerrado);
            resultado=resultado.replace(exponente+"√("+interior,"Math.pow("+interior+",1/"+exponente.toString());
        }
    }

    resultado = eval(resultado)
    resultado = parseFloat(resultado.toFixed(10))
    resultado = resultado.toString();
    resultado = resultado.replaceAll('.',',')
    return resultado
}
function cambiarSigno(cadena){
    cadena = cadena.replaceAll(',','.')
    if (cadena.charAt(cadena.length-1) != '/' && cadena.charAt(cadena.length-1) != 'x' &&
        cadena.charAt(cadena.length-1) != '^'){
        var soloNum=true
        for (var i=cadena.length-1; i>=0;i--){
            if (cadena.charAt(i) == '+'){
                cadena = cadena.substring(0,i)+'-'+cadena.substring(i+1)
                soloNum = false
                i=-1
            }else if (cadena.charAt(i) == '-'){
                if (cadena.length==1){
                    cadena = cadena.substring(1)
                }else if (cadena.charAt(i-1) == 'x' || cadena.charAt(i-1) == '/' ||
                    cadena.charAt(i-1) == '^' ||cadena.charAt(i-1) == '-'){
                    cadena = cadena.substring(0,i)+cadena.substring(i+1)
                }else {
                    cadena = cadena.substring(0,i)+'+'+cadena.substring(i+1)
                }
                soloNum = false
                i=-1
            }else if (cadena.charAt(i) == 'x' || cadena.charAt(i) == '/' || cadena.charAt(i) == '^'){
                cadena = cadena.slice(0,i+1)+(-1*parseFloat(cadena.substring(i+1))).toString()
                soloNum = false
                i=-1
            }
        }
        if (soloNum){
            cadena = '-'+cadena
        }
        if (cadena.charAt(0) == '+'){
            cadena=cadena.substring(1)
        }
    }
    cadena = cadena.replaceAll('x+','x')
    cadena = cadena.replaceAll('/+','/')
    cadena = cadena.replaceAll('^+','^')
    cadena = cadena.replaceAll('.',',')
    return cadena
}
