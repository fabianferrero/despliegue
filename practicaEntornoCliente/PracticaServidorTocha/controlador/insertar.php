<?php
require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');

$conexion = conectar();

$usuario =  $conexion->real_escape_string($_POST['usuario']);
$nombre = $conexion->real_escape_string($_POST['nombre']);
$apellidos = $conexion->real_escape_string($_POST['apellidos']);
$edad = $conexion->real_escape_string($_POST['edad']);
$telefono = $conexion->real_escape_string($_POST['telefono']);
$correo = $conexion->real_escape_string($_POST['correo']);
$contrasena = $conexion->real_escape_string($_POST['contraseña']);
$contrasena2 = $conexion->real_escape_string($_POST['contraseña2']);

if (isset($usuario) && !empty($usuario) &&
    isset($nombre) && !empty($nombre) &&
    isset($apellidos) && !empty($apellidos) &&
    isset($edad) && !empty($edad) &&
    isset($telefono) && !empty($telefono) &&
    isset($correo) && !empty($correo) &&
    isset($contrasena) && !empty($contrasena) &&
    isset($contrasena2) && !empty($contrasena2)){

    if ($contrasena == $contrasena2){

        $existe = $conexion->query("SELECT * FROM usuarios WHERE usuario = '$usuario'");
        $arrayUser = $existe->fetch_array();

        if (isset($arrayUser['id'])){
            header("Location:../vistas/registrar.php?error=usuario");
        }else{
            $sql = "INSERT INTO usuarios(usuario, nombre, apellidos, edad, telefono, correo, contrasena)
                    VALUES (?,?,?,?,?,?,?)";

            $stmt = $conexion->prepare($sql);
            $stmt->bind_param('sssisss', $usuario, $nombre, $apellidos, $edad, $telefono, $correo, $contrasena);
            $stmt->execute();
            $conexion->close();

            header("Location:../vistas/login.php");
        }
    }else{
        header("Location:../vistas/registrar.php?error=contra");
    }

}else{
    echo "Error al introducir datos";
}

?>