<?php
function cambiarContra($contrasena, $id){
    require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');
    $conexion = conectar();

    $sql = "UPDATE usuarios SET contrasena = ? WHERE id = ?";
    $stmt = $conexion->prepare($sql);
    $stmt->bind_param('si',$contrasena, $id);
    $stmt->execute();
    $stmt->close();
}
?>