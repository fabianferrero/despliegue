<?php
require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');
session_start();
$conexion = conectar();

$usuario = $conexion->real_escape_string($_POST['usuario']);
$contrasena = $conexion->real_escape_string($_POST['contraseña']);
$_SESSION['usuario']['datoIncorrecto'] = '';

if (isset($usuario) && !empty($usuario) &&
    isset($contrasena) && !empty($contrasena)){

    $sql = "SELECT * FROM usuarios WHERE usuario LIKE '$usuario'";

    $resultado = $conexion->query($sql);

    $arrayUser = $resultado->fetch_array();

    if (isset($arrayUser['id'])){
        if ($contrasena == $arrayUser['contrasena']){
            $_SESSION['usuario']['username'] = $usuario;
            $_SESSION['usuario']['id'] = $arrayUser[7];
            $conexion->close();

            $_SESSION['usuario']['loginTime'] = time();

            header("Location:../index/index.php");
        }else{
            $conexion->close();
            $_SESSION['usuario']['datoIncorrecto'] = 'contrasena';
            header("Location:../vistas/login.php");
        }
    }else{
        $_SESSION['usuario']['datoIncorrecto'] = 'usuario';
        header("Location:../vistas/login.php");
    }
}else{
    echo "<script>alert('datos incompletos')</script>";
    header("Location:../vistas/login.php");
}
?>