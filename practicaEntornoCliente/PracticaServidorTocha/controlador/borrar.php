<?php
function borrarUser($id){
    require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');

    $conexion = conectar();
    $sql = ('DELETE FROM usuarios WHERE id = ?');

    $stmt = $conexion->prepare($sql);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result=$stmt->close();
}
function borrarUserA($id){
    require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');

    $conexion = conectar();
    $sql = ('DELETE FROM usuarios WHERE id = ?');

    $stmt = $conexion->prepare($sql);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result=$stmt->close();
}
function borrarPiso($id){
    require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');

    $conexion = conectar();
    $sql = ('DELETE FROM pisos WHERE id = ?');

    $stmt = $conexion->prepare($sql);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $result=$stmt->close();
}
?>