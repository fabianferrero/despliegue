<?php

require ('fpdf/fpdf.php');
require_once '/xampp/htdocs/PracticaServidorTocha/controlador/admin/listarUsuariosPisos.php';
$datosPiso = piso($_POST['idPiso']);

$titulo = $datosPiso['titulo'];
$descripcion = $datosPiso['descripcion'];
$foto = $datosPiso['foto'];
$telefono = $datosPiso['telefono'];
$precio = $datosPiso['precio'];
$habitaciones = $datosPiso['habitaciones'];
$distancia = $datosPiso['distancia'];

$imgInsertar = $datosPiso['foto'];
$imagen = 'data://text/plain;base64,' . base64_encode($imgInsertar);

$pdf = new FPDF();
$pdf->SetAuthor('Fabian Ferrero');
$pdf->SetTitle('piso');
$pdf->AcceptPageBreak();
$pdf->AddPage();

$pdf->SetFont('Helvetica','',20);
$pdf->SetTextColor(244,98,58);
$pdf->Cell(30, 10, iconv('UTF-8', 'windows-1252', $titulo));
$pdf->Image($imagen,50,40,100,70,'jpg','');

$pdf->SetXY (50,110);
$pdf->SetFont('Helvetica','B',15);
$pdf->Cell(30, 10, iconv('UTF-8', 'windows-1252', 'DESCRIPCION:'));

$pdf->SetXY (50,150);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', 'TELEFONO:'));

$pdf->SetXY (50,170);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', 'HABITACIONES:'));


$pdf->SetXY (50,190);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', 'PRECIO:'));


$pdf->SetXY (50,210);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', 'DISTANCIA:'));

$pdf->SetFont('Helvetica','',20);
$pdf->SetTextColor(0,0,0);
$pdf->SetXY (49,120);
$pdf->SetFontSize(10);
$pdf->MultiCell(100, 3, iconv('UTF-8', 'windows-1252', $descripcion), 0,left);

$pdf->SetFont('Helvetica','',10);
$pdf->SetXY (100,150);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', $telefono));

$pdf->SetXY (100,170);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', $habitaciones));

$pdf->SetXY (100,190);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', $precio.'€'));

$pdf->SetXY (100,210);
$pdf->Cell(10, 10, iconv('UTF-8', 'windows-1252', $distancia.'m'));

ob_start();
//direccion
$pdf->Output('../pdfs/piso.pdf','I');

?>