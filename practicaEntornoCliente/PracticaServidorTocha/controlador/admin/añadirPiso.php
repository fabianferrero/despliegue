<?php
require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');

$conexion = conectar();

$titulo =  $conexion->real_escape_string($_POST['titulo']);
$habitaciones = $conexion->real_escape_string($_POST['habitaciones']);
$precio = $conexion->real_escape_string($_POST['precio']);
$descripcion = $conexion->real_escape_string($_POST['descripcion']);
$distancia = $conexion->real_escape_string($_POST['distancia']);
$telefono = $conexion->real_escape_string($_POST['telefono']);
$revisar = getimagesize($_FILES["imagen"]["tmp_name"]);

if($revisar !== false){

    $image = $_FILES['imagen']['tmp_name'];
    $imgContenido = addslashes(file_get_contents($image));

    if($conexion->connect_error){
        die("Connection failed: " . $conexion->connect_error);
    }
}

if (isset($titulo) && !empty($titulo) &&
    isset($habitaciones) && !empty($habitaciones) &&
    isset($precio) && !empty($precio) &&
    isset($descripcion) && !empty($descripcion) &&
    isset($distancia) && !empty($distancia) &&
    isset($telefono) && !empty($telefono)){

    $INSERTAR = $conexion->query("INSERT INTO pisos(titulo, habitaciones, precio, descripcion, distancia, telefono, foto)
                            VALUES ('$titulo','$habitaciones','$precio','$descripcion','$distancia','$telefono','$imgContenido')");
    $conexion->close();

    header("Location:../../vistas/admin/administracion.php");
}else{
    echo "Error al introducir datos";
}
?>