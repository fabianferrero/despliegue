<?php
require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');

session_start();
$conexion = conectar();

$titulo =  $conexion->real_escape_string($_POST['titulo']);
$habitaciones = $conexion->real_escape_string($_POST['habitaciones']);
$precio = $conexion->real_escape_string($_POST['precio']);
$descripcion = $conexion->real_escape_string($_POST['descripcion']);
$distancia = $conexion->real_escape_string($_POST['distancia']);
$telefono = $conexion->real_escape_string($_POST['telefono']);
$id = $conexion->real_escape_string($_POST['idPiso']);

if ($_FILES['imagenNueva']['name'] != ""){
    $revisar = getimagesize($_FILES["imagenNueva"]["tmp_name"]);

    if($revisar !== false){

        $image = $_FILES['imagenNueva']['tmp_name'];
        $imgContenido = addslashes(file_get_contents($image));

        if($conexion->connect_error){
            die("Connection failed: " . $conexion->connect_error);
        }
    }

    $sql = "UPDATE pisos SET foto = '$imgContenido' WHERE id = '$id'";
    $stmt = $conexion->prepare($sql);
    $stmt->execute();
    $result=$stmt->close();
}
if (isset($titulo) && !empty($titulo) &&
    isset($habitaciones) && !empty($habitaciones) &&
    isset($precio) && !empty($precio) &&
    isset($descripcion) && !empty($descripcion) &&
    isset($distancia) && !empty($distancia) &&
    isset($telefono) && !empty($telefono)){

    $sql = "UPDATE pisos SET titulo = ?, habitaciones = ?, precio = ?, descripcion = ?,
        distancia = ?, telefono = ? WHERE id = ?";
    $stmt = $conexion->prepare($sql);
    $stmt->bind_param('siisiii',$titulo,$habitaciones, $precio, $descripcion, $distancia, $telefono, $id);
    $stmt->execute();
    $result=$stmt->close();

    header("Location:../../vistas/admin/administracion.php?pagina=verPiso");
}else{
    echo "Error al introducir datos";
}
?>