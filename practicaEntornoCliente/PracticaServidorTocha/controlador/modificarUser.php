<?php
require_once('/xampp/htdocs/PracticaServidorTocha/modelo/conexion.php');
session_start();
$conexion = conectar();

$id = $_SESSION['usuario']['id'];
$usuario = $_POST['usuario'];
$nombre = $_POST['nombre'];
$apellidos = $_POST['apellidos'];
$edad = $_POST['edad'];
$telefono = $_POST['telefono'];
$correo = $_POST['correo'];

$sql = "UPDATE usuarios SET usuario = ?, nombre = ?, apellidos = ?, edad = ?,
        telefono = ?, correo = ? WHERE id = ?";
$stmt = $conexion->prepare($sql);
$stmt->bind_param('sssissi',$usuario,$nombre, $apellidos, $edad, $telefono, $correo, $id);
$stmt->execute();
$result=$stmt->close();

header("Location:../vistas/cuenta.php");

?>