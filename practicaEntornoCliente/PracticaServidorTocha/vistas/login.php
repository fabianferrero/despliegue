<html>
<head>
    <meta charset="UTF-8">
    <title>Pisos</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesLogin.css">
</head>
<body>
<?php
session_start();
if (isset($_SESSION['usuario']['datoIncorrecto'])){
    if ($_SESSION['usuario']['datoIncorrecto'] == 'usuario'){
        echo '<script>alert("usuario incorrecto")</script>';
    }else if ($_SESSION['usuario']['datoIncorrecto'] == 'contrasena'){
        echo '<script>alert("contrasena incorrecta")</script>';
    }
    unset($_SESSION['usuario']['datoIncorrecto']);
}
?>
<div class="login-page">
    <div class="form">
        <form class="login-form" action="../controlador/logear.php" method="post">
            <input type="text" placeholder="username" name="usuario" required/>
            <input type="password" placeholder="password" name="contraseña" required/>
            <button type="submit" name="entrar">entrar</button>
            <button><a href="../index/index.php">volver</a></button>
            <p class="message">No tienes cuenta?<a href="registrar.php">Crear cuenta</a></p>
        </form>
    </div>
</div>
</body>
</html>