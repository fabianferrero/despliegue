
<html>
<head>
    <meta charset="UTF-8">
    <title>Pisos</title>
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesLogin.css">
    <?php
    if (isset($_GET['error'])){
        if ($_GET['error'] == 'contra'){
            echo '<script>alert("Contraseñas no coinciden")</script>';
        }else if ($_GET['error'] == 'usuario'){
            echo '<script>alert("Usuario ya registrado")</script>';
        }
    }?>
</head>
<body>
<div class="register-page">
    <div class="form">
        <form class="login-form" method="post" action="../controlador/insertar.php">
            <input type="text" placeholder="usuario" name="usuario" required/>
            <input type="text" placeholder="nombre" name="nombre" required/>
            <input type="text" placeholder="apellidos" name="apellidos" required/>
            <input type="number" placeholder="edad" name="edad" required/>
            <input type="text" placeholder="telefono" name="telefono" required/>
            <input type="email" placeholder="correo" name="correo" required/>
            <input type="password" placeholder="contraseña" name="contraseña" required/>
            <input type="password" placeholder="confirmar contraseña" name="contraseña2" required/>
            <button type="submit">crear</button>
            <button><a href="../index/index.php">volver</a></button>
            <p class="message">Ya tienes cuenta?<a href="login.php">Iniciar sesión</a></p>
        </form>
    </div>
</div>
</body>
</html>