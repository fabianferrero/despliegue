<?php
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/deslogear.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/timeout.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/admin/listarUsuariosPisos.php');
session_start();
timeout();
$pisos=listarPisos();
$cantidadPisos = count($pisos);
$pisosPorPagina = 6;
$paginas =ceil($cantidadPisos/$pisosPorPagina);
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shop Homepage - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../assets/css/shop-homepage.css" rel="stylesheet">

</head>

<body>
    <?php if (!$_GET){
        header('Location:catalogo.php?pagina=1');
    } ?>
  <!-- Navigation -->
  <nav style="background-color: #f4623a" class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="../index/index.php">MONSTESSORI</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="catalogo.php?pagina=1" onclick="topFunction()" style="cursor: pointer;">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
              <?php
              if (isset($_SESSION['usuario']['id'])){
                  $usuario = $_SESSION['usuario']['username'];

                  echo "<li class='nav-item'>
                            <div class='dropdown'>
                                    <button style='background-color: transparent; border: none; margin-top: 7px' class='btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                        $usuario
                                    </button>
                                    <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                                    <form method='post'>
                                        <input type='submit' class='dropdown-item' name='cerrar' value='Cerrar Sesion' style='border: none; background-color: transparent;'>
                                    </form>
                                        <a class='dropdown-item' href='../vistas/cuenta.php'>Cuenta</a>
                                        <a class='dropdown-item' href='#'>Something else here</a>
                                    </div>
                                  </div>
                        </li>";
                  if (isset($_POST['cerrar'])){
                      deslogear();
                  }
              }
              ?>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <div class="col-lg-3">

        <h1 class="my-4">Montessori inmuebles</h1>


      </div>
      <!-- /.col-lg-3 -->

      <div class="col-lg-9">

        <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner" role="listbox" style="width: 870px">
            <?php
                echo '
            <div class="carousel-item active">
                <img src="data:image;base64,'.base64_encode($pisos[0]['foto']).'" style="width: 870px; height: 430px">
            </div>
            <div class="carousel-item">
                <img src="data:image;base64,'.base64_encode($pisos[1]['foto']).'" style="width: 870px; height: 430px">
            </div>  
            <div class="carousel-item">
                <img src="data:image;base64,'.base64_encode($pisos[2]['foto']).'" style="width: 870px; height: 430px">
            </div>     
                ';
            ?>
          </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="row">
            <?php
            $i = ($_GET['pagina']-1)*$pisosPorPagina;
            $cantidadPisos-6;
            if ($i+6<=$cantidadPisos){
                $pisosMostrar = $i+6;
            }else{
                $pisosMostrar=$cantidadPisos;
            }
            for ($i; $i<$pisosMostrar; $i++): ?>
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="verPiso.php?id=<?php echo $pisos[$i]['id'];?>"><?php echo '<img class="card-img-top" src="data:image;base64,'.base64_encode($pisos[$i]['foto']).'" style="width: 250px; height: 150px">';?></a>
              <div class="card-body">
                <h4 class="card-title">
                  <a href="#"><?php echo $pisos[$i]['titulo']?></a>
                </h4>
                <h5><?php echo $pisos[$i]['precio']?></h5>
                <p class="card-text"><?php echo $pisos[$i]['descripcion']?></p>
              </div>
              <div class="card-footer">
                <small class="text-muted">&#9733; &#9733; &#9733; &#9734; &#9734;</small>
              </div>
            </div>
          </div>
            <?php endfor; ?>
        </div>
        <!-- /.row -->
          <!-- desplazamiento -->
          <nav aria-label="Page navigation example" style="margin-left: 270px;">
              <ul class="pagination">
                  <li class="page-item <?php echo $_GET['pagina']<=1 ? 'disabled':''?>">
                      <a class="page-link" href="catalogo.php?pagina=<?php echo $_GET['pagina']-1;?>">Anterior</a>
                  </li>

                  <?php for ($i=0; $i<$paginas; $i++): ?>
                      <li class="page-item <?php echo $_GET['pagina']==$i+1 ? 'active':''?>">
                          <a class="page-link" href="catalogo.php?pagina=<?php echo $i+1?>">
                              <?php echo $i+1;?>
                          </a>
                      </li>
                  <?php endfor ?>

                  <li class="page-item <?php echo $_GET['pagina']>=$paginas ? 'disabled':''?>">
                      <a class="page-link" href="catalogo.php?pagina=<?php echo $_GET['pagina']+1;?>">Siguiente</a>
                  </li>
              </ul>
          </nav>
          <!-- /.desplazamiento -->
      </div>
      <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer style="background-color: #f4623a" class="py-5">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="/xampp/htdocs/PracticaServidorTocha/assets/js/jquery.min.js"></script>
  <script src="/xampp/htdocs/PracticaServidorTocha/assets/js/bootstrap.bundle.min.js"></script>
    <script>
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>
</html>
