<?php
session_start();
require_once('/xampp/htdocs/PracticaServidorTocha/controlador/admin/listarUsuariosPisos.php');
require_once('/xampp/htdocs/PracticaServidorTocha/controlador/deslogear.php');
require_once('/xampp/htdocs/PracticaServidorTocha/controlador/borrar.php');
require_once('/xampp/htdocs/PracticaServidorTocha/controlador/listarMensajes.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../assets/css/admin/tablaUsersAdmin.css" type="text/css">
    <link rel="stylesheet" href="../../assets/css/admin/formPisos.css" type="text/css">
</head>
<body>

<?php
$id=$_SESSION['admin']['idAdmin'];
$admin=$_SESSION['admin']['adminname'];
if (!isset($_SESSION['admin']['adminname'])){
    header('Location:loginAdmin.php');
}
if (!$_GET) {
    header('Location:administracion.php?pagina=usuarios');
}
?>

<nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#" style="color: white"><?php echo strtoupper($admin);?></a>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" id="botonTabla" href="administracion.php?pagina=usuarios" style="color: white; cursor: pointer">USUARIOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="botonPisos" href="administracion.php?pagina=addPiso" style="color: white; cursor: pointer">AÑADIR PISO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="botonVerPisos" href="administracion.php?pagina=verPiso" style="color: white; cursor: pointer">VER PISO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="botonVerMensajes" href="administracion.php?pagina=verMensajes" style="color: white; cursor: pointer">MENSAJES</a>
            </li>
            <li class="nav-item">
                <form method='post'>
                    <input type='submit' class='dropdown-item' name='cerrarAdmin' value='Cerrar Sesion' style='border: none; background-color: transparent; color: white; margin-top: 5px;'>
                </form>
                <?php
                if (isset($_POST['cerrarAdmin'])){
                    deslogearAdmin();
                }
                ?>
            </li>
        </ul>
    </div>
</nav>
<?php if ($_GET['pagina'] == 'usuarios'):?>
    <div id="tablaUsers" style="text-align: center">
        <br>
        <h1 style="color: white">USUARIOS</h1>
        <table class="tablaUsuarios" style="margin: auto; margin-top: 50px;">
            <tr class="filas">
                <td class="tablaBorde"></td>
                <td class="tablaBorde">ID</td>
                <td class="tablaBorde">USUARIO</td>
                <td class="tablaBorde">NOMBRE</td>
                <td class="tablaBorde">APELLIDOS</td>
                <td class="tablaBorde">EDAD</td>
                <td class="tablaBorde">TELEFONO</td>
                <td class="tablaBorde">CORREO</td>
            </tr>
            <?php
            $usuarios = listarUsusarios();
            foreach ($usuarios as $usuario) {
                echo '  <tr class="filas">
                    <td class="tablaBorde"><form method="post">
                    <input type="hidden" name="idUser" value="'.$usuario['id'].'">
                    <input class="eliminar" type="submit" value="ELIMINAR">
                    </form></td>
                    <td>' . $usuario['id'] . '</td>
                    <td>' . $usuario['usuario'] . '</td>
                    <td>' . $usuario['nombre'] . '</td>
                    <td>' . $usuario['apellidos'] . '</td>
                    <td>' . $usuario['edad'] . '</td>
                    <td>' . $usuario['telefono'] . '</td>
                    <td>' . $usuario['correo'] . '</td>
                </tr>';
            }
            if (isset($_POST['idUser'])){
                borrarUserA($_POST['idUser']);
                echo '<meta http-equiv="refresh" content="1">';
            }
            ?>
        </table>
    </div>
<?php endif;?>
<?php if ($_GET['pagina'] == 'addPiso'):?>
    <div id="addPisos" style="color: white; text-align: center">
        <h1>AÑADIR PISOS</h1>
        <form method="post" action="../../controlador/admin/añadirPiso.php" style="padding: 20px; margin: auto; width: 400px;" enctype="multipart/form-data">
            <div id="datos" style="padding: 10px; float: left; display: block">
                <div class="form-group">
                    <label>TITULO</label>
                    <input class="insertar" type="text" name="titulo" value="">
                </div>
                <div class="form-group">
                    <label>HABITACIONES</label>
                    <input class="insertar" type="text" name="habitaciones" value="">
                </div>
                <div class="form-group">
                    <label>PRECIO</label>
                    <input class="insertar" type="text" name="precio" value="">
                </div>
                <div class="form-group" style="height: 170px">
                    <label>DESCRIPCION</label>
                    <textarea class="insertar" style="height: 150px; resize: none" type="text" name="descripcion" value=""></textarea>
                </div>
                <div class="form-group">
                    <label>DISTANCIA(m)</label>
                    <input class="insertar" type="text" name="distancia" value="">
                </div>
                <div class="form-group">
                    <label>TELEFONO</label>
                    <input class="insertar" type="text" name="telefono" value="">
                </div>
                <div class="form-group">
                    <label>IMAGEN</label>
                    <span class="subirImagen">
                    <input type="file" class="insertar" style="height: 50px" name="imagen" id="imagen">
                </span>
                    <label for="imagen">Cargar imagen <br><b style="color: grey; font-size: 10px">MAX.(1920x1080)</b></label>
                </div>
                <br>
                <button type="submit" class="botonAñadir" style="width: 39%">AÑADIR</button>
            </div>
        </form>
    </div>
<?php endif;?>
<?php if ($_GET['pagina'] == 'verPiso'):?>
    <div id="verpisos" style="color: white; text-align: center; width: min-content; margin: auto">
        <h1>VER PISOS</h1>
        <form action="../../controlador/admin/eliminarPisos.php" method="post">
            <table class="tablaUsuarios">
                <tr class="filas">
                    <td class="tablaBorde"></td>
                    <td class="tablaBorde">ID</td>
                    <td class="tablaBorde">TITULO</td>
                    <td class="tablaBorde">HABITACIONES</td>
                    <td class="tablaBorde">PRECIO</td>
                    <td class="tablaBorde">DESCRIPCION</td>
                    <td class="tablaBorde">DISTANCIA</td>
                    <td class="tablaBorde">TELEFONO</td>
                    <td class="tablaBorde">IMAGEN</td>
                </tr>
                <?php
                if (listarPisos() != null){
                    $pisos = listarPisos();
                    foreach ($pisos as $key => $piso) {
                        $posicion = $key;
                        echo '  
                <tr class="filas">
                    <td class="tablaBorde">
                    <input class="eliminar" type="checkbox" name="'.$piso['id'].'">
                    </td>
                    <td>' . $piso['id'] . '</td>
                    <td>' . $piso['titulo'] . '</td>
                    <td>' . $piso['habitaciones'] . '</td>
                    <td>' . $piso['precio'] . '</td>
                    <td><textarea style="background-color: #091921; border: 10px; color: white; resize: none; height: 100px; width: 300px">' . $piso['descripcion'] . '</textarea></td>
                    <td>' . $piso['distancia'] . '</td>
                    <td>' . $piso['telefono'] . '</td>
                    <td><img src="data:image;base64,'.base64_encode($piso['foto']).'" style="width: 200px; height: 112px"></td>
                    <td><button type="submit" class="eliminar" name="botonMod" value="'.$piso['id'].'" style="width: 100%; height: 100%" formaction="administracion.php?pagina=modPiso&posicion='.$posicion.'">EDITAR</button></td>
                </tr>';
                    }
                }
                ?>
            </table>
            <input type="submit" name="eliminarPiso" value="ELIMINAR" class="eliminar" style="width: 100px; margin-left: 20px; margin-top: 10px; border: solid white">
        </form>
    </div>
<?php endif;?>
<?php if ($_GET['pagina'] == 'modPiso'):?>
    <?php
    if (listarPisos() != null) {
        $pisos = listarPisos();
    }
    $posicion = $_GET['posicion'];
    $titulo = $pisos[$posicion]['titulo'];
    $habitaciones = $pisos[$posicion]['habitaciones'];
    $precion = $pisos[$posicion]['precio'];
    $descripcion = $pisos[$posicion]['descripcion'];
    $distancia = $pisos[$posicion]['distancia'];
    $telefono = $pisos[$posicion]['telefono'];
    $idPiso = $pisos[$posicion]['id'];
    ?>
    <div id="modPiso" style="color: white; text-align: center">
        <h1>MODIFICAR PISO</h1>
        <form method="post" action="../../controlador/admin/modificarPiso.php" style="padding: 20px; margin: auto; width: 400px;" enctype="multipart/form-data">
            <div id="datos" style="padding: 10px; float: left; display: block">
                <div class="form-group" style="text-align: center">
                    <?php echo '<img src="data:image;base64,'.base64_encode($pisos[$posicion]['foto']).'" style="width: 400px; height: 212px">';?>
                    <input type="hidden" name="idPiso" value="<?php echo $idPiso?>">
                </div>
                <div class="form-group">
                    <label>TITULO</label>
                    <input class="insertar" type="text" name="titulo" value="<?php echo $titulo?>">
                </div>
                <div class="form-group">
                    <label>HABITACIONES</label>
                    <input class="insertar" type="text" name="habitaciones" value="<?php echo $habitaciones?>">
                </div>
                <div class="form-group">
                    <label>PRECIO</label>
                    <input class="insertar" type="text" name="precio" value="<?php echo $precion?>">
                </div>
                <div class="form-group" style="height: 170px">
                    <label>DESCRIPCION</label>
                    <textarea class="insertar" style="height: 150px; resize: none" type="text" name="descripcion" value=""><?php echo $descripcion?></textarea>
                </div>
                <div class="form-group">
                    <label>DISTANCIA(m)</label>
                    <input class="insertar" type="text" name="distancia" value="<?php echo $distancia?>">
                </div>
                <div class="form-group">
                    <label>TELEFONO</label>
                    <input class="insertar" type="text" name="telefono" value="<?php echo $telefono?>">
                </div>
                <div class="form-group">
                    <label>IMAGEN</label>
                    <span class="subirImagen">
                    <input type="file" class="insertar" style="height: 50px" name="imagenNueva" id="imagen" value="">
                </span>
                    <label for="imagen">Cargar imagen <br><b style="color: grey; font-size: 10px">MAX.(1920x1080)</b></label>
                </div>
                <br>
                <button type="submit" class="botonAñadir" style="width: 39%">GUARDAR</button>
            </div>
        </form>
    </div>
<?php endif;?>
<?php if ($_GET['pagina'] == 'verMensajes'):?>
<div id="verMensajes" style="color: white; text-align: center">
    <h1>Mensajes</h1>
    <table style="margin: auto; width: 900px; margin-top: 40px;">
        <tr style="background-color: #ff3077;">
            <td>USUARIO</td>
            <td>ID_PISO</td>
            <td>ASUNTO</td>
            <td></td>
        </tr>
        <?php
        $mensajes = mostrarMensajes();
        for ($i=0; $i<count($mensajes); $i++):?>
            <tr style="background-color: #0c2835">
                <td>
                    <strong><?php echo strtoupper($mensajes[$i]['usuario'])?></strong>
                </td>

                <td>
                    <strong><?php echo strtoupper($mensajes[$i]['id_piso'])?></strong>
                </td>
                <td>
                    <?php echo $mensajes[$i]['asunto']?>
                </td>
                <td>
                    <!-- modal1-->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $i;?>" style="outline: none; background-color: #ff3077; border: none">
                        Ver
                    </button>
                    <div class="modal fade" id="exampleModalCenter<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="color: black">
                                    <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $mensajes[$i]['asunto']?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="color: black">
                                    <?php echo $mensajes[$i]['mensaje']?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-dismiss="modal">Responder</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- !modal1-->
                    <!-- modal2-->
                    <form action="../../controlador/admin/añadirMensajeAdmin.php" method="post">
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="color: black">
                                        <h5 class="modal-title" id="exampleModalLabel">Nuevo Mensaje</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group" style="color: black">
                                                <label for="recipient-name" class="col-form-label">Asunto:</label>
                                                <input type="text" class="form-control" id="recipient-name" name="asunto">
                                                <input type="hidden" name="admin" value="<?php echo $_SESSION['admin']['adminname']?>">
                                            </div>
                                            <div class="form-group" style="color: black">
                                                <label for="message-text" class="col-form-label">Mensaje:</label>
                                                <textarea class="form-control" id="message-text" name="mensaje"></textarea>
                                                <input type="hidden" name="usuario" value="<?php echo $mensajes[$i]['usuario']?>">
                                                <input type="hidden" name="idPiso" value="<?php echo $mensajes[$i]['id_piso ']?>">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- !modal2-->
                </td>
            </tr>
        <?php endfor;?>
    </table>
</div>
<?php endif;?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="application/javascript">
    jQuery('input[type=file]').change(function(){
        var filename = jQuery(this).val().split('\\').pop();
        var idname = jQuery(this).attr('id');
        console.log(jQuery(this));
        console.log(filename);
        console.log(idname);
        jQuery('span.'+idname).next().find('span').html(filename);
    });
</script>
</body>
</html>