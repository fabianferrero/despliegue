<html>
<head>
    <meta charset="UTF-8">
    <title>Pisos</title>
    <link rel="stylesheet" type="text/css" href="../../assets/css/admin/loginAdmin.css">
</head>
<body>
<?php
session_start();
if (isset($_SESSION['admin']['datoIncorrecto'])){;
    if ($_SESSION['admin']['datoIncorrecto'] == 'usuario'){
        echo '<script>alert("usuario incorrecto")</script>';
    }else if ($_SESSION['admin']['datoIncorrecto'] == 'contrasena'){
        echo '<script>alert("contrasena incorrecta")</script>';
    }
    unset($_SESSION['admin']['datoIncorrecto']);
}
?>
<div class="login-page">
    <div class="form">
        <h1>ADMINISTRADOR</h1>
        <form class="login-form" action="../../controlador/admin/logearAdmin.php" method="post">
            <input type="text" placeholder="username" name="usuario" required/>
            <input type="password" placeholder="password" name="contraseña" required/>
            <button type="submit">entrar</button>
        </form>
    </div>
</div>
</body>
</html>