<?php
session_start();
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/mostrarUser.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/timeout.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/deslogear.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/modificarContraseña.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/listarMensajes.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/admin/listarMensajesAdmin.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/admin/listarUsuariosPisos.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/verFavoritos.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/añadirFavorito.php');
if (!isset($_SESSION['usuario'])){
    header("Location:/xampp/htdocs/PracticaServidorTocha/index/index.php");
}
timeout();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body style="background-color: darkgrey" onload="hide()">
<style>
    label{
        color: white;
    }
    input{
        width: 400px;
        border: none;
        border-radius: 7px;
        padding: 5px;
        display: block;
    }
    button, .modContra{
        background-color: #D95834;
        color: white;
        border: none;
        height: 40px;
        width: 100px;
        border-radius: 7px;
        padding: 5px;
    }
</style>
<?php
$id=$_SESSION['usuario']['id'];
$datosUser = datosUser($id);
$usuario = $datosUser['usuario'];
$nombre = $datosUser['nombre'];
$apellidos = $datosUser['apellidos'];
$edad = $datosUser['edad'];
$telefono = $datosUser['telefono'];
$correo = $datosUser['correo'];
$contrasena = $datosUser['contrasena'];
$contra='';
for ($i=0; $i<strlen($contrasena); $i++){
    $contra = $contra.'*';
}
if (!$_GET) {
    header('Location:cuenta.php?pagina=datos');
}
?>
<nav id="navegacion" class="navbar navbar-expand-lg navbar-light" style="background-color: #D95834">
    <a class="navbar-brand" href="../index/index.php" style="color: white">MONTESSORI</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="cuenta.php?pagina=datos" style="color: white">DATOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="cuenta.php?pagina=favoritos" style="color: white">FAVORITOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="cuenta.php?pagina=mensajes" style="color: white">MENSAJES</a>
            </li>
            <li class="nav-item">
                <form method="post">
                    <input type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style=" width: 100%; border: none; background-color: transparent; margin-top: 2px;" value="BORRAR CUENTA"/>
                </form>

                <?php
                require_once "/xampp/htdocs/PracticaServidorTocha/controlador/borrar.php";
                if (isset($_POST)){
                    echo '<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                ¿Estas seguro de que quieres eliminar tu cuenta?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                                                <form method="post">
                                                    <button class="btn btn-primary" name="seguro">SI</button>   
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                    if (isset($_POST['seguro'])){
                        borrarUser($id);
                        unset($_SESSION['usuario']);
                        header('Location:../index.php');
                    }
                }
                ?>
            </li>
            <li class="nav-item">
                <?php
                if (isset($_SESSION['usuario']['id'])){
                    $usuario = $_SESSION['usuario']['username'];

                    echo "<form method='post'>
                                <input type='submit' class='dropdown-item' name='cerrar' value='CERRAR SESION' style='color: white; margin-top: 4px; margin-left: -10px; border: none; background-color: transparent; cursor: pointer'>
                          </form>";
                    if (isset($_POST['cerrar'])){
                        deslogear();
                    }
                }
                ?>
            </li>
        </ul>
    </div>
</nav>

<h1 style="color: white"><?php echo $usuario;?></h1>

<?php if ($_GET['pagina'] == 'datos' || $_GET['pagina'] == 'datosContraseña'):?>
    <form method="post" action="../controlador/modificarUser.php" id="datosUsuario" style="position: relative; padding: 20px">
        <div id="datos" style="padding: 10px; float: left; display: block">
            <div class="form-group">
                <label for="exampleFormControlInput1">USUARIO</label>
                <input type="text" name="usuario" value="<?php echo $usuario?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">NOMBRE</label>
                <input type="text" name="nombre" value="<?php echo $nombre?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">APELLIDOS</label>
                <input type="text" name="apellidos" value="<?php echo $apellidos?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">EDAD</label>
                <input type="text" name="edad" value="<?php echo $edad?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">TELEFONO</label>
                <input type="text" name="telefono" value="<?php echo $telefono?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">CORREO</label>
                <input type="text" name="correo" value="<?php echo $correo?>">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">CONRASEÑA</label>
                <br>
                <input type="text" readonly style="display: inline" value="<?php echo $contra?>">
                <button class="modContra" type="button" style="display: inline"><a style="color: white" href="cuenta.php?pagina=datosContraseña">MODIFICAR</a></button>
            </div>
            <button type="submit" style="width: 39%">APLICAR</button>
            <button type="button" style="width: 39%" onclick="window.location.href='../index/index.php'">VOLVER</button>
        </div>
    </form>
    <?php if ($_GET['pagina'] == 'datosContraseña'):?>
        <form method="post">
            <div id="contra" style="padding: 10px; float: left;">
                <div class="form-group">
                    <label for="exampleFormControlInput1">ANTERIOR CONTRASEÑA</label>
                    <input type="text" name="viejaCon" placeholder="antigua contraseña">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">NUEVA CONTRASEÑA</label>
                    <input type="text" name="nuevaCon" placeholder="nueva contraseña">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">CONFIRMAR CONTRASEÑA</label>
                    <input type="text" name="nuevaCon2" placeholder="nueva contraseña">
                </div>
                <button type="submit" name="aplicarContra" style="width: 45%" onclick="window.location.href='cuenta.php?pagina=datos';">APLICAR</button>
                <button type="button" style="float: right; width: 45%"><a style="color: white" href="cuenta.php?pagina=datos">VOLVER</a></button>
                <?php
                if (isset($_POST['nuevaCon'])){
                    if ($_POST['viejaCon'] == $contrasena){
                        if ($_POST['nuevaCon'] == $_POST['nuevaCon2']){
                            cambiarContra($_POST['nuevaCon'], $id);
                            echo '<meta http-equiv="refresh" content="0.5">';
                            unset($_POST['nuevaCon']);
                        }else{
                            echo '<script>alert("Las contraseñas no coinciden")</script>';
                        }
                    }else{
                        echo '<script>alert("contraseña incorrecta")</script>';
                    }
                }
                ?>
            </div>
        </form>
    <?php endif;?>
<?php endif;?>
<?php
$mensajesEnviados = mostrarMensajes();
$mensajesRecividos = mostrarMensajesAdmin();
$indiceModal = 0;
if ($_GET['pagina'] == 'mensajes'):?>
    <div style="text-align: center">
        <h1 style="color: white">MENSAJES</h1>
        <table style="margin: auto; width: 900px; color: white;">
            <tr style="background-color: #D95834">
                <td colspan="3">ENVIADOS</td>
            </tr>
            <tr>
                <td style="float: left; padding: 10px;"><h3>ASUNTO</h3></td>
            </tr>
            <?php for ($i=0; $i<count($mensajesEnviados); $i++):?>
                <?php if ($mensajesEnviados[$i]['usuario'] == $_SESSION['usuario']['username']):
                    $indiceModal++;
                    ?>
                <tr>
                    <td  style="padding: 10px; float: left"><?php echo $mensajesEnviados[$i]['asunto']?></td>
                    <td>
                        <form action="../controlador/eliminarMensaje.php" method="post">
                            <input type="hidden" name="idMensaje" value="<?php echo $mensajesEnviados[$i]['id']?>">
                            <button type="submit" style="float: right; background-color: #D95834; border: none">
                                ELIMINAR
                            </button>
                        </form>
                    </td>
                    <td  style="padding: 10px; width: 100px">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $indiceModal?>" style="float: right; background-color: #D95834; border: none">
                            VER
                        </button>
                        <div class="modal fade" id="exampleModalCenter<?php echo $indiceModal?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="color: black">
                                        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $mensajesEnviados[$i]['asunto']?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style="color: black">
                                        <?php echo $mensajesEnviados[$i]['mensaje']?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- !modal1-->
                    </td>
                </tr>
                <?php endif;?>
            <?php endfor;?>
            <tr style="background-color: #D95834">
                <td colspan="3">RECIVIDOS</td>
            </tr>
            <tr>
                <td style="float: left; padding: 10px;"><h3>ASUNTO</h3></td>
            </tr>
            <?php for ($i=0; $i<count($mensajesRecividos); $i++):?>
                <?php if ($mensajesRecividos[$i]['usuario'] == $_SESSION['usuario']['username']):
                    $indiceModal++;
                    ?>

                    <tr>
                        <td  style="padding: 10px; float: left"><?php echo $mensajesRecividos[$i]['asunto']?></td>
                        <td></td>
                        <td  style="padding: 10px;">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $indiceModal?>" style="float: right; background-color: #D95834">
                                VER
                            </button>
                            <div class="modal fade" id="exampleModalCenter<?php echo $indiceModal?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="color: black">
                                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $mensajesRecividos[$i]['asunto']?></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style="color: black">
                                            <?php echo $mensajesRecividos[$i]['mensaje']?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalEnviar" data-dismiss="modal">Responder</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- !modal1-->
                            <!-- modal2-->
                            <form action="../controlador/introducirMensaje.php" method="post">
                                <div class="modal fade" id="exampleModalEnviar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header" style="color: black">
                                                <h5 class="modal-title" id="exampleModalLabel">Nuevo Mensaje</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="form-group" style="color: black">
                                                        <label for="recipient-name" class="col-form-label" style="color: black; float: left">Asunto:</label>
                                                        <input type="text" class="form-control" id="recipient-name" name="asunto">
                                                    </div>
                                                    <div class="form-group" style="color: black">
                                                        <label for="message-text" class="col-form-label" style="color: black; float: left">Mensaje:</label>
                                                        <textarea class="form-control" id="message-text" name="mensaje"></textarea>
                                                        <input type="hidden" name="usuario" value="<?php echo $mensajesRecividos[$i]['usuario']?>">
                                                        <input type="hidden" name="idPiso" value="<?php echo $mensajesRecividos[$i]['id_piso']?>">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-primary">Enviar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php
                endif;?>
            <?php endfor;?>
        </table>
    </div>
<?php endif;?>
<?php if ($_GET['pagina'] == 'favoritos'):?>
    <table style="margin: auto; width: 900px; color: white;">
        <tr style="background-color: #D95834; text-align: center">
            <td colspan="4">TUS PISOS FAVORITOS</td>
        </tr>
        <?php
        $favoritos = verFavoritos($usuario);
        if ($favoritos != null){
        for ($i=0; $i<count($favoritos); $i++):
            $piso = piso($favoritos[$i]['id_piso']);
            ?>
            <tr style="text-align: center">
                <td rowspan="2" style="padding: 10px; width: 100px"><?php echo '<img class="card-img-top" src="data:image;base64,'.base64_encode($piso['foto']).'" style="width: 250px; height: 130px">';?></td>
                <td rowspan="2"><?php echo '<strong>'.strtoupper($piso['titulo']).'</strong> <br>'.$piso['descripcion']?></td>
                <td style="width: 100px;">
                    <form method="post">
                        <input type="hidden" name="idPiso" value="<?php echo $favoritos[$i]['id_piso']?>">
                        <button type="submit" style="cursor: pointer"><a>QUITAR</a></button>
                    </form>
                </td>
            </tr>
            <tr>
                <td style="width: 100px;">
                    <button type="submit" style="cursor: pointer;"><a style="text-decoration: none; color: white" href="verPiso.php?id=<?php echo $favoritos[$i]['id_piso']?>">VER</a></button>
                </td>
            </tr>
        <?php endfor;}?>
    </table>
<?php endif;?>
<?php if (isset($_POST['idPiso'])){
    eliminarFav($usuario,$_POST['idPiso']);
    echo '<meta http-equiv="refresh" content="0.5">';
}?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" src="../assets/js/modificarContra.js"></script>
</body>
</html>
