<?php
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/deslogear.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/timeout.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/admin/listarUsuariosPisos.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/mostrarComentarios.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/existeFavorito.php');
require_once ('/xampp/htdocs/PracticaServidorTocha/controlador/añadirFavorito.php');
session_start();
if (!isset($_SESSION['usuario']['username'])){
    header("Location:catalogo.php");
}
timeout();
$url="http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$_SESSION['piso']['url']=$url;
$id=$_GET['id'];
$piso=piso($id);
$comentarios=mostrar($id);
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/shop-homepage.css" rel="stylesheet">
    <link type="text/css" href="../assets/css/favorito.css">
</head>

<body>
<!-- Navigation -->
<nav style="background-color: #f4623a" class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="../index/index.php">MONTESSORI</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="catalogo.php?pagina=1" style="font-size: 16px">Inicio</a>
                </li>
                <li class="nav-item">
                    <?php
                    if (isset($_SESSION['usuario']['id'])){
                        $usuario = $_SESSION['usuario']['username'];

                        echo "<li class='nav-item'>
                            <div class='dropdown'>
                                    <button style='font-family: Arial; background-color: transparent; border: none; margin-top: 18px' class='btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                        <strong>$usuario</strong>
                                    </button>
                                    <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                                    <form method='post'>
                                        <input type='submit' class='dropdown-item' name='cerrar' value='Cerrar Sesion' style='border: none; background-color: transparent;'>
                                    </form>
                                        <a class='dropdown-item' class='dropdown-item' href='../vistas/cuenta.php' style='font-size: 16px'>Cuenta</a>
                                    </div>
                                  </div>
                        </li>";
                        if (isset($_POST['cerrar'])){
                            deslogear();
                        }
                    }
                    ?>
            </ul>
        </div>
    </div>
</nav>

<div class="piso">

    <table>
        <tr>
            <td class="info"><?php echo '<img class="card-img-top" src="data:image;base64,'.base64_encode($piso['foto']).'" style="width: 900px; height: 550px">';?></td>
        </tr>
        <tr>
            <td class="info"><strong style="font-size: 40px"><?php echo $piso['titulo'];?></strong></td>
        </tr>
        <tr>
            <td class="info"><h5><strong>Descripción: </strong></h5><?php echo $piso['descripcion'];?></td>
        </tr>
        <tr>
            <td class="info"><h5><strong>Nº Habitaciones: </strong></h5><?php echo $piso['habitaciones'];?></td>
        </tr>
        <tr>
            <td class="info"><h5><strong>Distancia a Montessori: </strong></h5><?php echo $piso['distancia'];?> metros</td>
        </tr>
        <tr>
            <td class="info"><h5><strong>Telefono: </strong></h5><?php echo $piso['telefono'];?></td>
        </tr>
        <tr>
            <td class="info"><h5><strong>Precio: </strong></h5><?php echo $piso['precio'];?>€</td>
        </tr>
        <tr>
            <td>
                <!-- favorito-->
                <form method="post">
                    <input type="hidden" name="fav">
                    <button type="submit" id="añadirFavorito" style="visibility: hidden"></button>
                </form>
                <?php
                if (isset($_POST['fav'])){
                    añadirFavOno($usuario, $piso['id']);
                }
                ?>
                <div id="main-content">
                    <div>
                        <input type="checkbox" id="checkbox" onclick="favorito()"
                            <?php
                            if (existe($usuario,$piso['id'])){echo 'checked';}
                                ?>/>
                        <label for="checkbox">
                            <svg id="heart-svg" viewBox="467 392 58 57" xmlns="http://www.w3.org/2000/svg"><g id="Group" fill="none" fill-rule="evenodd" transform="translate(467 392)"><path d="M29.144 20.773c-.063-.13-4.227-8.67-11.44-2.59C7.63 28.795 28.94 43.256 29.143 43.394c.204-.138 21.513-14.6 11.44-25.213-7.214-6.08-11.377 2.46-11.44 2.59z" id="heart" fill="#AAB8C2"/><circle id="main-circ" fill="#E2264D" opacity="0" cx="29.5" cy="29.5" r="1.5"/><g id="grp7" opacity="0" transform="translate(7 6)"><circle id="oval1" fill="#9CD8C3" cx="2" cy="6" r="2"/><circle id="oval2" fill="#8CE8C3" cx="5" cy="2" r="2"/></g><g id="grp6" opacity="0" transform="translate(0 28)"><circle id="oval1" fill="#CC8EF5" cx="2" cy="7" r="2"/><circle id="oval2" fill="#91D2FA" cx="3" cy="2" r="2"/></g><g id="grp3" opacity="0" transform="translate(52 28)"><circle id="oval2" fill="#9CD8C3" cx="2" cy="7" r="2"/><circle id="oval1" fill="#8CE8C3" cx="4" cy="2" r="2"/></g><g id="grp2" opacity="0" transform="translate(44 6)" fill="#CC8EF5"><circle id="oval2" transform="matrix(-1 0 0 1 10 0)" cx="5" cy="6" r="2"/><circle id="oval1" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"/></g><g id="grp5" opacity="0" transform="translate(14 50)" fill="#91D2FA"><circle id="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"/><circle id="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"/></g><g id="grp4" opacity="0" transform="translate(35 50)" fill="#F48EA7"><circle id="oval1" transform="matrix(-1 0 0 1 12 0)" cx="6" cy="5" r="2"/><circle id="oval2" transform="matrix(-1 0 0 1 4 0)" cx="2" cy="2" r="2"/></g><g id="grp1" opacity="0" transform="translate(24)" fill="#9FC7FA"><circle id="oval1" cx="2.5" cy="3" r="2"/><circle id="oval2" cx="7.5" cy="2" r="2"/></g></g></svg>
                        </label>
                    </div>
                </div>
                <script type="text/javascript">
                    function favorito(){
                        setTimeout(function (){
                            document.getElementById('añadirFavorito').click();
                        }, 1000)

                    }
                </script>
                <!-- !favorito-->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" STYLE="margin-bottom: 10px; height: 40px; float: right; margin-right: 10px; background-color: #f4623a; border: none">Mensaje</button>
                <form action="../controlador/pdf/crearPdf.php" method="post">
                    <input type="hidden" name="idPiso" value="<?php echo $piso['id']?>">
                    <button type="submit" class="btn btn-primary" STYLE=" height: 40px; width: 50px; float: right; margin-right: 10px; background-color: #f4623a; border: none; background-image: url('../assets/img/icon/impresora.png'); background-position: center; background-repeat: no-repeat"></button>
                </form>
                <form action="../controlador/introducirMensaje.php" method="post">
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Mensaje</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Asunto:</label>
                                            <input type="text" class="form-control" id="recipient-name" name="asunto">
                                            <input type="hidden" name="usuario" value="<?php echo $_SESSION['usuario']['username']?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">Mensaje:</label>
                                            <textarea class="form-control" id="message-text" name="mensaje"></textarea>
                                            <input type="hidden" name="idPiso" value="<?php echo $piso['id']?>">
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </td>
        </tr>
    </table>
</div>
<div class="piso">
    <div style="height: 200px">
        <form action="../controlador/añadirComentario.php" method="post">
            <input type="submit" class="comentar" value="Comentar">
            <textarea class="cuadroComentario" placeholder="Escribe un comentario" name="comentario"></textarea>
            <input type="hidden" name="usuario" value="<?php echo $_SESSION['usuario']['username']?>">
            <input type="hidden" name="piso" value="<?php echo $piso['id']?>">
            <input type="hidden" name="header" value="<?php echo $piso?>">
        </form>
    </div>
    <table style="width: 900px;">
        <?php for ($i=0; $i<count($comentarios); $i++):?>
        <tr>
            <td><h5><strong><?php echo $comentarios[$i]['usuario']?>:</strong></h5></td>
        </tr>
        <tr>
            <td style="padding-bottom: 20px"><?php echo $comentarios[$i]['mensaje']?></td>
        </tr>
        <?php endfor?>
    </table>
</div>
<!-- Footer -->
<footer style="background-color: #f4623a" class="py-5">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.bundle.min.js"></script>

<style>
    .info{
        padding: 20px;
        word-break: break-all;
    }
    table{
        margin: auto;
        text-align: left;
        border-radius: 10px;
        box-shadow: -10px -10px 15px rgba(0, 0, 0, 0.2),
        10px 10px 15px rgba(0, 0, 0, 0.2);
    }
    .piso{
        margin: auto;
        text-align: center;
        margin-top: 50px;
        margin-bottom: 50px;
        width: 900px;
    }
    .comentar{
        background-color: #f4623a;
        border-radius: 10px;
        border: none;
        color: white;
        height: 50px;
        margin-left: 20px;
        margin-top: 10px;
        position: relative;
        float: left
    }
    .cuadroComentario{
        width: 750px;
        border-radius: 10px;
        resize: none;
    }
    body {
        margin: 0;
        padding: 0;
        -webkit-font-smoothing: antialiased;
        font-family: arial;
    }

    #main-content {
        display: flex;
        width: 50px;
        height: 50px;
        background: white;
        align-items: center;
        justify-content: center;
        text-align: center;
        float: right;
        margin-top: -5px;
        margin-right: 5px;
    }

    h1 {
        font-size: 14px;
        font-weight: 400;
        margin: 10px 0 0 0;
        color: #888888;
    }

    a {
        font-size: 12px;
        font-weight: bold;
        margin-top: 10px;
        display: inline-block;
        text-decoration: none;
        color: #008A68;
    }

    svg {
        cursor: pointer;
        overflow: visible;
        width: 60px;
    }
    svg #heart {
        transform-origin: center;
        animation: animateHeartOut .3s linear forwards;
    }
    svg #main-circ {
        transform-origin: 29.5px 29.5px;
    }

    #checkbox {
        display: none;
    }

    #checkbox:checked + label svg #heart {
        transform: scale(0.2);
        fill: #E2264D;
        animation: animateHeart .3s linear forwards .25s;
    }
    #checkbox:checked + label svg #main-circ {
        transition: all 2s;
        animation: animateCircle .3s linear forwards;
        opacity: 1;
    }
    #checkbox:checked + label svg #grp1 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp1 #oval1 {
        transform: scale(0) translate(0, -30px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp1 #oval2 {
        transform: scale(0) translate(10px, -50px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp2 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp2 #oval1 {
        transform: scale(0) translate(30px, -15px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp2 #oval2 {
        transform: scale(0) translate(60px, -15px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp3 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp3 #oval1 {
        transform: scale(0) translate(30px, 0px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp3 #oval2 {
        transform: scale(0) translate(60px, 10px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp4 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp4 #oval1 {
        transform: scale(0) translate(30px, 15px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp4 #oval2 {
        transform: scale(0) translate(40px, 50px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp5 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp5 #oval1 {
        transform: scale(0) translate(-10px, 20px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp5 #oval2 {
        transform: scale(0) translate(-60px, 30px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp6 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp6 #oval1 {
        transform: scale(0) translate(-30px, 0px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp6 #oval2 {
        transform: scale(0) translate(-60px, -5px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp7 {
        opacity: 1;
        transition: .1s all .3s;
    }
    #checkbox:checked + label svg #grp7 #oval1 {
        transform: scale(0) translate(-30px, -15px);
        transform-origin: 0 0 0;
        transition: .5s transform .3s;
    }
    #checkbox:checked + label svg #grp7 #oval2 {
        transform: scale(0) translate(-55px, -30px);
        transform-origin: 0 0 0;
        transition: 1.5s transform .3s;
    }
    #checkbox:checked + label svg #grp2 {
        opacity: 1;
        transition: .1s opacity .3s;
    }
    #checkbox:checked + label svg #grp3 {
        opacity: 1;
        transition: .1s opacity .3s;
    }
    #checkbox:checked + label svg #grp4 {
        opacity: 1;
        transition: .1s opacity .3s;
    }
    #checkbox:checked + label svg #grp5 {
        opacity: 1;
        transition: .1s opacity .3s;
    }
    #checkbox:checked + label svg #grp6 {
        opacity: 1;
        transition: .1s opacity .3s;
    }
    #checkbox:checked + label svg #grp7 {
        opacity: 1;
        transition: .1s opacity .3s;
    }

    @keyframes animateCircle {
        40% {
            transform: scale(10);
            opacity: 1;
            fill: #DD4688;
        }
        55% {
            transform: scale(11);
            opacity: 1;
            fill: #D46ABF;
        }
        65% {
            transform: scale(12);
            opacity: 1;
            fill: #CC8EF5;
        }
        75% {
            transform: scale(13);
            opacity: 1;
            fill: transparent;
            stroke: #CC8EF5;
            stroke-width: .5;
        }
        85% {
            transform: scale(17);
            opacity: 1;
            fill: transparent;
            stroke: #CC8EF5;
            stroke-width: .2;
        }
        95% {
            transform: scale(18);
            opacity: 1;
            fill: transparent;
            stroke: #CC8EF5;
            stroke-width: .1;
        }
        100% {
            transform: scale(19);
            opacity: 1;
            fill: transparent;
            stroke: #CC8EF5;
            stroke-width: 0;
        }
    }
    @keyframes animateHeart {
        0% {
            transform: scale(0.2);
        }
        40% {
            transform: scale(1.2);
        }
        100% {
            transform: scale(1);
        }
    }
    @keyframes animateHeartOut {
        0% {
            transform: scale(1.4);
        }
        100% {
            transform: scale(1);
        }
    }
</style>

</body>

</html>
