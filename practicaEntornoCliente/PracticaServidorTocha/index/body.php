
<body>
<!-- Masthead-->
<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-lg-10 align-self-end">
                <h1 class="text-uppercase text-white font-weight-bold">Pisos y apartamentos <br> al mejor precio</h1>
                <hr class="divider my-4" />
            </div>
            <div class="col-lg-8 align-self-baseline">
                <p class="text-white-75 font-weight-light mb-5">Empieza a buscar tu nuevo hogar!</p>
                <?php
                if (isset($_SESSION['usuario']['username'])){
                    echo "<p><h2 style='color: white'>BIENVENIDO ".strtoupper($usuario)."</h2></p>";
                }else{
                    echo "<a class='btn btn-primary btn-xl js-scroll-trigger' href='../vistas/login.php'>Entrar</a>";
                }
                ?>
            </div>
        </div>
    </div>
</header>
<!-- Nosotros-->
<section class="page-section bg-primary" id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="text-white mt-0">Tenemos lo que estas buscando!</h2>
                <hr class="divider light my-4" />
                <p class="text-white-50 mb-4">Ponemos a tu disposicion un amplio repertorio de inmuebles te todo tipo para asegurarte que busques lo que busque lo encontrarás.</p>
            </div>
        </div>
    </div>
</section>
<!-- Carrusel-->
<div style="text-align: center" id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="3000">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="../assets/img/pisos/1.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="../assets/img/pisos/2.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="../assets/img/pisos/3.jpg" alt="Third slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- Call to action-->
<section id="catalogo" class="page-section bg-dark text-white">
    <div class="container text-center">
        <h2 class="mb-4">ENCUENTRA TU NUEVO HOGAR</h2>
        <a class="btn btn-light btn-xl" href="../vistas/catalogo.php">VER PISOS</a>
    </div>
</section>
<!-- Contact-->
<section class="page-section" id="contact">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="mt-0">CONTACTANOS</h2>
                <hr class="divider my-4" />
                <p class="text-muted mb-5">¿Listo para comenzar tu próximo proyecto con nosotros? ¡Llámanos o envíanos un correo electrónico y nos pondremos en contacto contigo lo antes posible!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                <div>+1 (555) 123-4567</div>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                <!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                <a class="d-block" href="">contact@yourwebsite.com</a>
            </div>
        </div>
    </div>
</section>
</body>