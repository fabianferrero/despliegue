
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Practica Servidor</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../assets/css/styles.css" rel="stylesheet" />
</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Colegio Monstessori</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">Nosotros</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#catalogo">Catalogo</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contacto</a></li>
                <?php
                if (isset($_SESSION['usuario']['id'])){
                    $usuario = $_SESSION['usuario']['username'];

                    echo "<li class='nav-item'>
                                        <div class='dropdown'>
                                    <button style='font-family: Arial; background-color: transparent; border: none;' class='btn-secondary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                        <strong>$usuario</strong>
                                    </button>
                                    <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                                    <form method='post'>
                                        <input type='submit' class='dropdown-item' name='cerrar' value='Cerrar Sesion' style='border: none; background-color: transparent;'>
                                    </form>
                                        <a class='dropdown-item' href='../vistas/cuenta.php'>Cuenta</a>
                                        <a class='dropdown-item' href='#'>Something else here</a>
                                    </div>
                                  </div>
                                  </li>";
                    if (isset($_POST['cerrar'])){
                        deslogear();
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
</body>